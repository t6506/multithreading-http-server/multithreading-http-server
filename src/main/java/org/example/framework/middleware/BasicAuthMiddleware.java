package org.example.framework.middleware;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.exception.BadAuthenticationException;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.LoginPrincipal;
import org.example.framework.http.HttpHeaders;
import org.example.framework.http.Request;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


@Slf4j
@RequiredArgsConstructor
public class BasicAuthMiddleware implements Middleware {
  private static final String SCHEME = "Basic ";
  private final Authenticator authenticator;

  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    try {
      String headerAuth = request.getHeaders().get(HttpHeaders.AUTHORIZATION.value().toLowerCase());
      if (headerAuth == null) {
        log.error("not authenticated");
        return;
      }
      if (!headerAuth.startsWith(SCHEME)) {
        log.error("not authenticated");
        return;
      }
      byte[] decoded = Base64.getDecoder().decode(headerAuth.substring(SCHEME.length()));
      String[] parts = new String(decoded, StandardCharsets.UTF_8).split(":", 2);
      if (parts.length != 2) {
        log.error("invalid authorization header: {}", headerAuth);
        throw new BadAuthenticationException("invalid authorization header format");
      }
      final String login = parts[0];
      final String password = parts[1];

      LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }

      SecurityContext.setPrincipal(new LoginPrincipal(login));
      log.debug("authenticated: {}", login);


    } catch (Exception e) {
      SecurityContext.clear();
      log.error("not authenticated");
    }
  }
}
