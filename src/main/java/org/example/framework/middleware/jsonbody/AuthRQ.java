package org.example.framework.middleware.jsonbody;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AuthRQ {
    private final String login;
    private final String password;
}
